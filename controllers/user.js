const User = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const userService = require('../service/user')

exports.register = async (req, res, next) => {
    try {
        const userBody = req.body;
        const userData = await userService.register(userBody);

        // if https => secure: true
        res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})

        return res.status(200).json({
            success: true,
            userData,
            message: "success"
        });
    } catch (err) {
        console.log(err);
        return res.status(400).json({success: false, message: err.message});
    }
}

exports.login = async (req, res, next) => {
    try {
        const {email, password} = req.body;
        const userData = await userService.login(email, password);

        res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})

        return res.status(200).json({
            success: true,
            userData,
            message: "success"
        });
    } catch (err) {
        console.log(err);
        return res.status(400).json({success: false, message: err.message});
    }
}

exports.logout = async (req, res, next) => {
    try {
        const {refreshToken} = res.cookies;
        await userService.logout(refreshToken);
        res.clearCookie('refreshToken');

        return res.json(200);
    } catch (err) {
        console.log(err);
        return res.status(400).json({success: false, message: err.message});
    }
}

exports.refresh = async (req, res, next) => {
    try {
        const {refreshToken} = res.cookies;
        const userData = await userService.refresh(refreshToken);

        res.cookie('refreshToken', userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true})

        return res.status(200).json({
            success: true,
            userData,
            message: "success"
        });
    } catch (err) {
        console.log(err);
        return res.status(400).json({success: false, message: err.message});
    }
}

exports.profile = async (req, res, next) => {
    res.json({
        isAuth: true,
        id: req.user._id,
        email: req.user.email,
        name: req.user.firstname + req.user.lastname
    })
}

exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.find();
        res.json(users)
    } catch (err) {
        res.json({message: err})
    }
}

exports.addUser = async (req, res, next) => {
    const {
        username,
        firstname,
        lastname,
        email,
        password,
        model_id,
    } = req.body;

    const user = new User({
        username,
        firstname,
        lastname,
        email,
        password,
        model_id,
    });

    try {
        const savedUser = await user.save();
        if (savedUser) {
            const users = await User.find();
            res.json(users)
        }
    } catch (err) {
        res.json({message: err})
    }
}

exports.getUserById = async (req, res, next) => {
    try {
        const findUser = await User.findById(req.params.userId).populate('models');

        if (!findUser) {
            return res.status(400).json({success: false, message: "No user with such id"});
        }

        res.status(200).json(findUser)
    } catch (err) {
        res.status(400).json({message: err})
    }
}

exports.getUserByEmail = async (req, res, next) => {
    try {
        const findUser = await User.findOne({email: req.params.email});
        if (!findUser) {
            return res.status(400).json({success: false, message: "No user with such email"});
        }

        return res.status(200).json(findUser);
    } catch (err) {
        console.log(err)
        return res.json({message: err})
    }
}

exports.deleteUser = async (req, res, next) => {
    try {
        const deleteUser = await User.deleteOne({_id: req.params.userId});
        if (deleteUser) {
            const users = await User.find();
            res.json(users)
        }
    } catch (err) {
        res.json({message: err})
    }
}

exports.updateUser = async (req, res, next) => {
    const userId = req.params.id;
    try {
        const updateUser = await User.updateOne(
            {userId},
            {
                $set: req.body
            }
        );
        if (updateUser) {
            const user = await User.findOne({_id: req.params.userId});
            res.json(user)
        }
    } catch (err) {
        res.json({message: err})
    }
}

exports.updateUserModel = async (req, res, next) => {
    const {model} = req.body;
    try {
        const updateUser = await User.updateOne({_id: req.params.userId}, {
            $set: {
                model_id: model,
            }
        });
        if (updateUser) {
            const user = await User.findOne({_id: req.params.userId});
            res.status(200).json(user)
        }
    } catch (err) {
        res.json({message: err})
    }
}

exports.updateUserTokenAmount = async (req, res, next) => {
    const {tokenAmount, walletTokenAmount} = req.body;

    try {
        const findUser = await User.findById(req.params.userId).populate('models');

        if (!findUser) {
            return res.status(400).json({success: false, message: "No user with such id"});
        }

        findUser.tokenAmount = tokenAmount;
        findUser.walletTokenAmount = walletTokenAmount;
        findUser.save();
        res.json(findUser);
    } catch (err) {
        res.json({message: err})
    }
}

exports.updateUserResources = async (req, res, next) => {
    const {waterResource, earthResource, fireResource} = req.body;

    try {
        const findUser = await User.findById(req.params.userId).populate('models');

        if (!findUser) {
            return res.status(400).json({success: false, message: "No user with such id"});
        }

        findUser.waterResource += parseInt(waterResource, 10);
        findUser.earthResource += parseInt(earthResource, 10);
        findUser.fireResource += parseInt(fireResource);
        findUser.save();
        res.json(findUser);
    } catch (err) {
        res.json({message: err})
    }
}