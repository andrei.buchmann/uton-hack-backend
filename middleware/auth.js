const tokenService = require('../service/token');

exports.auth = async (req, res, next) => {
    next()
    try {
        const unity_password = req.body.unity_password;

        if (unity_password && unity_password === process.env.UNITY_PASSWORD) {
            next()
        }

        const authorizationHeader = req.headers.authorization;

        if (!authorizationHeader) {
            return res.status(401).json({success: false, message: 'User is not authorized'});
        }

        const accessToken = authorizationHeader.split(' ')[1];
        if (!accessToken) {
            return res.status(401).json({success: false, message: 'User is not authorized'});
        }

        const user = tokenService.validateAccessToken(accessToken);
        if (!user) {
            return res.status(401).json({success: false, message: 'User is not authorized'});
        }

        next();
    } catch (err) {
        console.log(err);
        return res.status(500).json({success: false, message: err.message});
    }
}