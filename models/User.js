const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = mongoose.Schema({
    username: {type: String, required: true},
    firstname: {type: String, required: true, maxlength: 100},
    lastname: {type: String, required: true, maxlength: 100},
    email: {type: String, required: true, trim: true, unique: 1},
    password: {type: String, required: true,},
    password_repeat: {type: String, required: true,},
    walletAddress: {type: String, required: true,},
    tokenAmount: {type: Number, default: 5000},
    walletTokenAmount: {type: Number, default: 0},
    waterResource: {type: Number, default: 1},
    earthResource: {type: Number, default: 1},
    fireResource: {type: Number, default: 1},
    models: {type: Schema.Types.ObjectId, ref: "Model"},
}, {timestamps: {createdAt: 'created_at'}});

module.exports = mongoose.model('User', User);